package com.homework.activities.data

import app.cash.sqldelight.db.SqlDriver
import app.cash.sqldelight.driver.native.NativeSqliteDriver
import com.homework.activities.db.ActivitiesDatabase

actual class SqlDelightDriverFactory {
    actual fun createDriver(): SqlDriver {
        return NativeSqliteDriver(ActivitiesDatabase.Schema, DATABASE_NAME)
    }
}