package com.homework.activities.di

import com.homework.activities.data.SqlDelightDriverFactory
import org.koin.core.module.Module
import org.koin.dsl.module

actual fun platformModule(): Module = module {
    single { SqlDelightDriverFactory().createDriver() }
}