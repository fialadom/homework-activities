package com.homework.activities.di

import app.cash.sqldelight.db.SqlDriver
import com.homework.activities.data.SqlDelightDriverFactory
import org.koin.core.module.Module
import org.koin.dsl.module

actual fun platformModule(): Module = module {
    single<SqlDriver> {
        SqlDelightDriverFactory(context = get()).createDriver()
    }
}