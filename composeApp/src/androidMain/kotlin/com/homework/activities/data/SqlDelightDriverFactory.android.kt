package com.homework.activities.data

import android.content.Context
import app.cash.sqldelight.db.SqlDriver
import app.cash.sqldelight.driver.android.AndroidSqliteDriver
import com.homework.activities.db.ActivitiesDatabase

actual class SqlDelightDriverFactory(private val context: Context) {
    actual fun createDriver(): SqlDriver {
        return AndroidSqliteDriver(ActivitiesDatabase.Schema, context, DATABASE_NAME)
    }
}