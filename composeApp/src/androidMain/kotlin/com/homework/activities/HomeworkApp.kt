package com.homework.activities

import android.app.Application
import com.homework.activities.di.initKoin
import org.koin.android.ext.koin.androidContext

class HomeworkApp :Application() {

    override fun onCreate() {
        super.onCreate()
        initKoin {
            androidContext(this@HomeworkApp)
        }
    }
}