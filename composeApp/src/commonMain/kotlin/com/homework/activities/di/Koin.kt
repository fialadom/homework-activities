package com.homework.activities.di

import org.koin.core.context.startKoin
import org.koin.dsl.KoinAppDeclaration

fun initKoin(appDeclaration: KoinAppDeclaration = {}) = startKoin {
    appDeclaration()
    modules(
        platformModule(), commonModule(), networkModule()
    )
}

fun initKoin() {
    initKoin { }
}