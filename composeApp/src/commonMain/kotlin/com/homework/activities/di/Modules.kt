package com.homework.activities.di

import com.homework.activities.data.ActivityRepository
import com.homework.activities.data.ActivityRepositoryImpl
import com.homework.activities.data.local.IntegerAdapter
import com.homework.activities.data.local.LocalDataSource
import com.homework.activities.data.local.LocalDataSourceImpl
import com.homework.activities.data.remote.ActivitiesApi
import com.homework.activities.data.remote.RemoteDataSource
import com.homework.activities.data.remote.RemoteDataSourceImpl
import com.homework.activities.data.remote.httpClient
import com.homework.activities.db.ActivitiesDatabase
import com.homework.activities.db.ActivitiesQueries
import com.homework.activities.db.Activity
import com.homework.activities.screens.detail.ActivityDetailViewModel
import com.homework.activities.screens.list.ActivitiesListViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.IO
import org.koin.compose.viewmodel.dsl.viewModelOf
import org.koin.core.module.Module
import org.koin.core.module.dsl.singleOf
import org.koin.dsl.module

expect fun platformModule(): Module

fun commonModule() = module {
    single {
        ActivitiesDatabase.invoke(
            driver = get(), activityAdapter = Activity.Adapter(
                durationAdapter = IntegerAdapter
            )
        )
    }
    single<ActivitiesQueries> {
        get<ActivitiesDatabase>().activitiesQueries
    }

    single<LocalDataSource> {
        LocalDataSourceImpl(
            activitiesQueries = get()
        )
    }
    viewModelOf(::ActivitiesListViewModel)
    factory { (recordId: String?, isLocal: Boolean) ->
        ActivityDetailViewModel(
            recordId = recordId, isLocal = isLocal, activityRepository = get()
        )
    }
    factory {
        CoroutineScope(Dispatchers.IO)
    }
    single<ActivityRepository> {
        ActivityRepositoryImpl(
            localDataSource = get(), remoteDataSource = get(), scope = get()
        )
    }
}

fun networkModule() = module {
    singleOf(::httpClient)
    singleOf(::ActivitiesApi)
    single<RemoteDataSource> {
        RemoteDataSourceImpl(
            api = get()
        )
    }
}