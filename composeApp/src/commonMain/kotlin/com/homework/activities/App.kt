package com.homework.activities

import androidx.compose.runtime.Composable
import androidx.navigation.compose.rememberNavController
import com.homework.activities.ui.navigation.AppNavHost
import com.homework.activities.ui.theme.AppTheme
import org.jetbrains.compose.ui.tooling.preview.Preview
import org.koin.compose.KoinContext

@Composable
@Preview
fun App() {
    KoinContext {
        AppTheme {
            val navController = rememberNavController()
            AppNavHost(
                navController = navController
            )
        }
    }
}