package com.homework.activities.ui.navigation

const val ARGUMENT_ACTIVITY_ID = "activityId"
const val ARGUMENT_IS_LOCAL = "isLocal"

sealed class Screen(val route: String) {
    object ActivitiesList: Screen("activities_list_route")
    object ActivityDetail: Screen("activity_detail_route")
}
