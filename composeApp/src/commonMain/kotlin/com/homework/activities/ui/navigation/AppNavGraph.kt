package com.homework.activities.ui.navigation

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.navArgument
import com.homework.activities.data.entities.SportActivity
import com.homework.activities.screens.detail.ActivityDetailRoute
import com.homework.activities.screens.detail.navigateToActivityDetail
import com.homework.activities.screens.list.ActivitiesListRoute
import org.koin.compose.viewmodel.koinViewModel
import org.koin.core.annotation.KoinExperimentalAPI
import org.koin.core.parameter.parametersOf

@Composable
fun AppNavHost(
    paddingValues: PaddingValues = PaddingValues(),
    navController: NavHostController,
    startDestination: String = Screen.ActivitiesList.route
) {
    NavHost(
        modifier = Modifier.padding(paddingValues),
        navController = navController,
        startDestination = startDestination
    ) {
        activitiesListScreen(
            navigateToActivityDetail = navController::navigateToActivityDetail
        )
        activityDetailScreen(
            navigateBack = navController::navigateUp
        )
    }
}

fun NavGraphBuilder.activitiesListScreen(navigateToActivityDetail: (SportActivity?) -> Unit) {
    composable(route = Screen.ActivitiesList.route) {
        ActivitiesListRoute(
            navigateToActivityDetail = navigateToActivityDetail
        )
    }
}

@OptIn(KoinExperimentalAPI::class)
fun NavGraphBuilder.activityDetailScreen(navigateBack: () -> Unit) {
    composable(
        route = "${Screen.ActivityDetail.route}/{$ARGUMENT_ACTIVITY_ID}/{$ARGUMENT_IS_LOCAL}",
        arguments = listOf(navArgument(ARGUMENT_ACTIVITY_ID) {
            type = NavType.StringType
            defaultValue = null
            nullable = true
        }, navArgument(ARGUMENT_IS_LOCAL) {
            type = NavType.BoolType
            defaultValue = true
        })
    ) { backStackEntry ->
        val recordId = backStackEntry.arguments?.getString(ARGUMENT_ACTIVITY_ID)
        val isLocal = backStackEntry.arguments?.getBoolean(ARGUMENT_IS_LOCAL)
        ActivityDetailRoute(navigateBack = navigateBack,
            viewModel = koinViewModel { parametersOf(recordId, isLocal) })
    }
}