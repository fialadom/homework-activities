package com.homework.activities.data.remote

import com.homework.activities.data.entities.SportActivity
import com.homework.activities.data.entities.toDomain
import com.homework.activities.data.entities.toNetwork
import kotlinx.coroutines.flow.Flow

class RemoteDataSourceImpl(
    private val api: ActivitiesApi
) : RemoteDataSource {
    override fun getAllActivities(): Flow<Resource<List<SportActivity>>> {
        return safeApiCall {
            api.getAllActivities().map {
                it.toDomain()
            }
        }
    }

    override fun getActivityById(remoteId: String): Flow<Resource<SportActivity>> {
        return safeApiCall {
            api.getActivityById(remoteId).toDomain()
        }
    }

    override fun postActivity(sportActivity: SportActivity): Flow<Resource<SportActivity>> {
        return safeApiCall {
            api.createActivity(sportActivity.toNetwork()).toDomain()
        }
    }

    override fun patchActivity(sportActivity: SportActivity): Flow<Resource<SportActivity>> {
        return safeApiCall {
            api.updateActivity(sportActivity.toNetwork()).toDomain()
        }
    }

    override fun deleteActivity(remoteId: String): Flow<Resource<Boolean>> {
        return safeApiCall {
            val response = api.deleteActivity(remoteId)
            response.result.any { it == remoteId }
        }
    }
}