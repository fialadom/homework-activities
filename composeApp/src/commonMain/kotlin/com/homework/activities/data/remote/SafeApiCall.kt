package com.homework.activities.data.remote

import io.ktor.client.plugins.ResponseException
import io.ktor.client.statement.bodyAsText
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow

fun <T : Any> safeApiCall(
    remoteCall: suspend () -> T
): Flow<Resource<T>> {
    return flow<Resource<T>> {
        emit(Resource.Success(remoteCall()))
    }.catch {
        when (it) {
            is ResponseException -> {
                emit(Resource.Failure(it.response.bodyAsText()))
            }

            else -> {
                emit(Resource.Failure(it.message ?: it.toString()))
            }
        }
    }
}

sealed class Resource<out T> {
    class Success<T>(val data: T) : Resource<T>()
    class Failure(val error: String) : Resource<Nothing>()
}
