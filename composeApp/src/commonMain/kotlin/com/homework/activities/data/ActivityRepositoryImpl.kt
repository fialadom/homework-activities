package com.homework.activities.data

import com.homework.activities.data.entities.SportActivity
import com.homework.activities.data.local.LocalDataSource
import com.homework.activities.data.remote.RemoteDataSource
import com.homework.activities.data.remote.Resource
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

class ActivityRepositoryImpl(
    private val localDataSource: LocalDataSource,
    private val remoteDataSource: RemoteDataSource,
    private val scope: CoroutineScope
) : ActivityRepository {

    private val _allRemoteActivities = MutableStateFlow<List<SportActivity>>(emptyList())
    override val allRemoteActivities = _allRemoteActivities.onStart {
        initRemoteRecords() //precache
    }

    override val allLocalActivities = localDataSource.getAllActivities()

    override fun getActivityById(id: String?, isLocal: Boolean): Flow<Resource<SportActivity>> {
        val activity = if (id == null) {
            flowOf(
                Resource.Success(SportActivity.Empty)
            )
        } else if (isLocal) {
            localDataSource.getActivityById(id.toLong()).map {
                Resource.Success(it)
            }
        } else {
            remoteDataSource.getActivityById(id)
        }
        return activity
    }

    override fun updateActivity(activity: SportActivity): Flow<Resource<Any>> {
        return if (activity.isLocal) {
            localDataSource.upsertActivity(activity)
            flowOf(Resource.Success(true))
        } else {
            if (activity.remoteId != null) {
                remoteDataSource.patchActivity(activity).onEach { result ->
                    if (result is Resource.Success) {
                        updateRemoteRecord(result.data)
                    }
                }
            } else {
                remoteDataSource.postActivity(activity)
            }.onEach { result ->
                if (result is Resource.Success) {
                    updateRemoteRecord(result.data)
                }
            }
        }
    }

    override fun deleteActivity(activity: SportActivity): Flow<Resource<Boolean>> {
        return if (activity.isLocal) {
            localDataSource.deleteActivity(activity.id!!)
            flowOf(Resource.Success(true))
        } else {
            remoteDataSource.deleteActivity(activity.remoteId!!).onEach { result ->
                if (result is Resource.Success) {
                    deleteRemoteRecord(sportActivity = activity)
                }
            }
        }
    }

    private fun initRemoteRecords() {
        scope.launch {
            remoteDataSource.getAllActivities().map { result ->
                when (result) {
                    is Resource.Failure -> {}
                    is Resource.Success -> _allRemoteActivities.update { result.data }
                }
            }.collect()
        }
    }

    private fun updateRemoteRecord(sportActivity: SportActivity) {
        val records = _allRemoteActivities.value.toMutableList()
        val index = records.indexOfFirst { it.remoteId == sportActivity.remoteId }
        if (index == -1) { //
            records.add(sportActivity)
        } else { //update
            records[index] = sportActivity
        }
        _allRemoteActivities.update { records }
    }

    private fun deleteRemoteRecord(sportActivity: SportActivity) {
        val records = _allRemoteActivities.value.toMutableList()
        val index = records.indexOfFirst { it.remoteId == sportActivity.remoteId }
        records.removeAt(index)
        _allRemoteActivities.update { records }
    }
}
