package com.homework.activities.data.local

import app.cash.sqldelight.ColumnAdapter

object IntegerAdapter : ColumnAdapter<Int, Long> {
    override fun decode(databaseValue: Long): Int {
        return databaseValue.toInt()
    }

    override fun encode(value: Int): Long {
        return value.toLong()
    }
}
