package com.homework.activities.data.remote

import com.homework.activities.data.entities.DeleteResponseDto
import com.homework.activities.data.entities.SportActivityDto
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.request
import io.ktor.client.request.setBody
import io.ktor.http.HttpMethod

class ActivitiesApi(
    private val httpClient: HttpClient
) {
    suspend fun getAllActivities(): List<SportActivityDto> {
        return httpClient.request("/rest/activities") {
            method = HttpMethod.Get
        }.body()
    }

    suspend fun getActivityById(id: String): SportActivityDto {
        return httpClient.request("/rest/activities/$id") {
            method = HttpMethod.Get
        }.body()
    }

    suspend fun createActivity(sportActivity: SportActivityDto): SportActivityDto {
        return httpClient.request("/rest/activities") {
            method = HttpMethod.Post
            setBody(sportActivity)
        }.body()
    }

    suspend fun updateActivity(sportActivity: SportActivityDto): SportActivityDto {
        return httpClient.request("/rest/activities/${sportActivity._id}") {
            method = HttpMethod.Patch
            setBody(sportActivity)
        }.body()
    }

    suspend fun deleteActivity(id: String): DeleteResponseDto {
        return httpClient.request("/rest/activities/$id") {
            method = HttpMethod.Delete
        }.body()
    }

}