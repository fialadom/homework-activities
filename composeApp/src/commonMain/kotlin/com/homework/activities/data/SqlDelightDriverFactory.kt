package com.homework.activities.data

import app.cash.sqldelight.db.SqlDriver

const val DATABASE_NAME = "ActivitiesDatabase"

expect class SqlDelightDriverFactory {
    fun createDriver(): SqlDriver
}