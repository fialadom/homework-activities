package com.homework.activities.data.local

import app.cash.sqldelight.coroutines.asFlow
import app.cash.sqldelight.coroutines.mapToList
import app.cash.sqldelight.coroutines.mapToOne
import com.homework.activities.data.entities.SportActivity
import com.homework.activities.db.ActivitiesQueries
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.IO
import kotlinx.coroutines.flow.Flow

class LocalDataSourceImpl(
    private val activitiesQueries: ActivitiesQueries
) : LocalDataSource {
    override fun getAllActivities(): Flow<List<SportActivity>> {
        return activitiesQueries.allActivities(::localToDomain).asFlow().mapToList(Dispatchers.IO)
    }

    override fun getActivityById(id: Long): Flow<SportActivity> {
        return activitiesQueries.activityById(id, ::localToDomain).asFlow().mapToOne(Dispatchers.IO)
    }

    override fun upsertActivity(sportActivity: SportActivity) {
        activitiesQueries.upsert(
            activityId = sportActivity.id,
            name = sportActivity.name,
            place = sportActivity.place,
            duration = sportActivity.duration ?: 0
        )
    }

    override fun deleteActivity(id: Long) {
        activitiesQueries.deleteById(id)
    }
}
