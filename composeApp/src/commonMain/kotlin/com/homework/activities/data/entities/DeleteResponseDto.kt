package com.homework.activities.data.entities


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class DeleteResponseDto(
    @SerialName("result")
    val result: List<String>
)