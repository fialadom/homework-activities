package com.homework.activities.data.remote

import io.ktor.client.HttpClient
import io.ktor.client.plugins.contentnegotiation.ContentNegotiation
import io.ktor.client.plugins.defaultRequest
import io.ktor.client.plugins.logging.LogLevel
import io.ktor.client.plugins.logging.Logger
import io.ktor.client.plugins.logging.Logging
import io.ktor.client.plugins.logging.SIMPLE
import io.ktor.client.request.header
import io.ktor.http.ContentType
import io.ktor.http.HttpHeaders
import io.ktor.serialization.kotlinx.json.json
import kotlinx.serialization.json.Json

@OptIn(kotlinx.serialization.ExperimentalSerializationApi::class)
fun httpClient(): HttpClient {
    val client = HttpClient {
        defaultRequest { //defaults
            url(NetworkConstants.baseUrl)
            header(HttpHeaders.Accept, ContentType.Application.Json)
            header(HttpHeaders.ContentType, ContentType.Application.Json)
            header("X-ApiKey", NetworkConstants.api_key)
        }
        install(Logging) { //Communication logger
            logger = Logger.SIMPLE
            level = LogLevel.ALL
        }
        install(ContentNegotiation) { //content to JSON
            json(Json {
                prettyPrint = true
                ignoreUnknownKeys = true
            })
        }
    }
    return client
}