package com.homework.activities.data.entities


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class SportActivityDto(
    @SerialName("_id") val _id: String? = null,
    @SerialName("name") val name: String,
    @SerialName("place") val place: String,
    @SerialName("duration") val duration: Int,
    @SerialName("id") val id: Int? = null
)

fun SportActivityDto.toDomain(): SportActivity {
    return SportActivity(
        remoteId = _id, name = name, place = place, duration = duration, isLocal = false
    )
}

fun SportActivity.toNetwork(): SportActivityDto {
    return SportActivityDto(
        _id = remoteId,
        name = name,
        place = place,
        duration = duration ?: 0,
    )
}
