package com.homework.activities.data

import com.homework.activities.data.entities.SportActivity
import com.homework.activities.data.remote.Resource
import kotlinx.coroutines.flow.Flow

interface ActivityRepository {
    val allRemoteActivities: Flow<List<SportActivity>>
    val allLocalActivities: Flow<List<SportActivity>>
    fun getActivityById(id: String?, isLocal: Boolean): Flow<Resource<SportActivity>>
    fun updateActivity(activity: SportActivity): Flow<Resource<Any>>
    fun deleteActivity(activity: SportActivity): Flow<Resource<Boolean>>
}