package com.homework.activities.data.remote

import com.homework.activities.data.entities.SportActivity
import kotlinx.coroutines.flow.Flow

interface RemoteDataSource {

    fun getAllActivities() : Flow<Resource<List<SportActivity>>>

    fun getActivityById(remoteId: String): Flow<Resource<SportActivity>>

    fun postActivity(sportActivity: SportActivity): Flow<Resource<SportActivity>>

    fun patchActivity(sportActivity: SportActivity): Flow<Resource<SportActivity>>

    fun deleteActivity(remoteId: String): Flow<Resource<Boolean>>
}