package com.homework.activities.data.local

import com.homework.activities.data.entities.SportActivity
import kotlinx.coroutines.flow.Flow

interface LocalDataSource {

    fun getAllActivities() : Flow<List<SportActivity>>

    fun getActivityById(id: Long): Flow<SportActivity>

    fun upsertActivity(sportActivity: SportActivity)

    fun deleteActivity(id: Long)
}