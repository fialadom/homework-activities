package com.homework.activities.data.entities

data class SportActivity(
    val id: Long? = null,
    val name: String,
    val place: String,
    val duration: Int? = null,
    val remoteId: String? = null,
    val isLocal: Boolean = true
) {
    companion object {
        val Empty = SportActivity(
            name = "", place = "", duration = 0, isLocal = true
        )
    }

    val durationInHours = (duration ?: 0) / 60
    val durationInHoursAndMinutes = duration?.minus(durationInHours * 60)
}