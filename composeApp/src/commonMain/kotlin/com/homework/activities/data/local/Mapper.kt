package com.homework.activities.data.local

import com.homework.activities.data.entities.SportActivity

fun localToDomain(
    activityId: Long,
    name: String,
    place: String,
    duration: Int,
) = SportActivity(
    id = activityId, name = name, place = place, duration = duration
)