package com.homework.activities.screens.detail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.homework.activities.data.ActivityRepository
import com.homework.activities.data.entities.SportActivity
import com.homework.activities.data.remote.Resource
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

class ActivityDetailViewModel(
    private val recordId: String?,
    private val isLocal: Boolean = true,
    private val activityRepository: ActivityRepository
) : ViewModel() {

    private val _isEditMode = MutableStateFlow(recordId != null)
    private val _sportActivity = MutableStateFlow<SportActivity?>(null)
    private val _errorState = MutableStateFlow(ErrorState())
    val errorState = _errorState.asStateFlow()
    private val _jobDone = MutableSharedFlow<Boolean>()
    val jobDone = _jobDone.asSharedFlow()

    val state = combine(
        _isEditMode, _sportActivity
    ) { isEditMode, activity ->
        ActivityDetailUiState(
            isLoading = false, sportActivity = activity, isEditMode = isEditMode
        )
    }.onStart {
        getInitialValues()
    }.stateIn(
        scope = viewModelScope,
        started = SharingStarted.WhileSubscribed(5000),
        initialValue = ActivityDetailUiState()
    )

    private fun getInitialValues() {
        viewModelScope.launch {
            activityRepository.getActivityById(recordId, isLocal).catch {
                println("getInitialValues.error=${it.message}")
            }.collect { result ->
                when (result) {
                    is Resource.Failure -> {
                        _errorState.update { it.copy(serverError = result.error) }
                    }

                    is Resource.Success -> _sportActivity.update { result.data }
                }
            }
        }
    }

    fun onTitleChanged(title: String) {
        _sportActivity.update {
            it?.copy(
                name = title
            )
        }
        _errorState.update {
            it.copy(
                isTitleEmpty = title.isEmpty()
            )
        }
    }

    fun onPlaceChanged(place: String) {
        _sportActivity.update {
            it?.copy(
                place = place
            )
        }
        _errorState.update {
            it.copy(
                isPlaceEmpty = place.isEmpty()
            )
        }
    }

    fun onDurationChanged(duration: String) {
        val value = duration.toIntOrNull()
        _sportActivity.update {
            it?.copy(
                duration = value
            )
        }
        _errorState.update {
            it.copy(
                isDurationInvalid = value == null || value <= 0
            )
        }
    }

    fun onLocationChanged(isLocal: Boolean) {
        _sportActivity.update {
            it?.copy(
                isLocal = isLocal
            )
        }
    }

    fun onSave() {
        val errors = _errorState.value
        if (errors.isPlaceEmpty || errors.isTitleEmpty || errors.isDurationInvalid) {
            return
        }

        viewModelScope.launch {
            activityRepository.updateActivity(_sportActivity.value!!).collect { result ->
                when (result) {
                    is Resource.Failure -> _errorState.update { it.copy(serverError = result.error) }
                    is Resource.Success -> _jobDone.emit(true)
                }
            }
        }
    }

    fun onDelete() {
        viewModelScope.launch {
            _errorState.update { it.copy(serverError = null) }
            activityRepository.deleteActivity(_sportActivity.value!!).collect { result ->
                when (result) {
                    is Resource.Failure -> _errorState.update { it.copy(serverError = result.error) }
                    is Resource.Success -> _jobDone.emit(true)
                }
            }
        }
    }
}


data class ActivityDetailUiState(
    val isLoading: Boolean = true,
    val sportActivity: SportActivity? = null,
    val isEditMode: Boolean = true,
)

data class ErrorState(
    val isTitleEmpty: Boolean = false,
    val isPlaceEmpty: Boolean = false,
    val isDurationInvalid: Boolean = false,
    val serverError: String? = null,
)