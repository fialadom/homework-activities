package com.homework.activities.screens.list

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.homework.activities.data.ActivityRepository
import com.homework.activities.data.entities.SportActivity
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.flow.update

class ActivitiesListViewModel(
    activityRepository: ActivityRepository
) : ViewModel() {

    private val selectedFilter = MutableStateFlow(FilterType.ALL)

    val state = combine(
        selectedFilter,
        activityRepository.allRemoteActivities,
        activityRepository.allLocalActivities
    ) { filter, remoteRecords, localRecords ->
        val records = when (filter) {
            FilterType.ALL -> localRecords + remoteRecords
            FilterType.LOCAL -> localRecords
            FilterType.REMOTE -> remoteRecords
        }
        ActivitiesListUiState(
            activities = records
        )
    }.stateIn(
        scope = viewModelScope,
        started = SharingStarted.WhileSubscribed(5000),
        initialValue = ActivitiesListUiState()
    )

    fun onFilterChanged(filter: FilterType) {
        selectedFilter.update { filter }
    }
}

data class ActivitiesListUiState(
    val activities: List<SportActivity> = emptyList(),
    val error: String? = null,
    val selectedFilter: FilterType = FilterType.ALL
)

enum class FilterType {
    ALL, LOCAL, REMOTE
}