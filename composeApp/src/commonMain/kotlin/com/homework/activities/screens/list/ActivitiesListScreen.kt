package com.homework.activities.screens.list

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FabPosition
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SuggestionChip
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.homework.activities.data.entities.SportActivity
import homeworkactivities.composeapp.generated.resources.Res
import homeworkactivities.composeapp.generated.resources.ic_local_24
import homeworkactivities.composeapp.generated.resources.ic_remote_24
import homeworkactivities.composeapp.generated.resources.local
import homeworkactivities.composeapp.generated.resources.main_title
import homeworkactivities.composeapp.generated.resources.no_records
import homeworkactivities.composeapp.generated.resources.pretty_duration_long
import homeworkactivities.composeapp.generated.resources.pretty_duration_short
import homeworkactivities.composeapp.generated.resources.record_location
import homeworkactivities.composeapp.generated.resources.remote
import org.jetbrains.compose.resources.painterResource
import org.jetbrains.compose.resources.stringResource
import org.koin.compose.viewmodel.koinViewModel
import org.koin.core.annotation.KoinExperimentalAPI

@OptIn(KoinExperimentalAPI::class)
@Composable
fun ActivitiesListRoute(
    navigateToActivityDetail: (SportActivity?) -> Unit,
    viewModel: ActivitiesListViewModel = koinViewModel<ActivitiesListViewModel>()
) {
    val uiState by viewModel.state.collectAsState()
    ActivitiesListScreen(
        uiState = uiState,
        navigateToActivityDetail = navigateToActivityDetail,
        onFilterChanged = viewModel::onFilterChanged
    )
}

@OptIn(ExperimentalMaterial3Api::class, ExperimentalFoundationApi::class)
@Composable
fun ActivitiesListScreen(
    uiState: ActivitiesListUiState,
    navigateToActivityDetail: (SportActivity?) -> Unit,
    onFilterChanged: (FilterType) -> Unit,
) {
    Scaffold(topBar = {
        TopAppBar(title = {
            Text(stringResource(Res.string.main_title))
        }, actions = {
            FilterButton(filterType = FilterType.ALL, onClick = {
                onFilterChanged(FilterType.ALL)
            })
            FilterButton(filterType = FilterType.LOCAL, onClick = {
                onFilterChanged(FilterType.LOCAL)
            })
            FilterButton(filterType = FilterType.REMOTE, onClick = {
                onFilterChanged(FilterType.REMOTE)
            })

        })
    }, floatingActionButton = {
        FloatingActionButton(onClick = {
            navigateToActivityDetail(null)
        }) {
            Icon(
                imageVector = Icons.Default.Add, contentDescription = "Add"
            )
        }
    }, floatingActionButtonPosition = FabPosition.End
    ) { paddingValues ->
        LazyColumn(
            modifier = Modifier.padding(paddingValues).padding(8.dp),
            verticalArrangement = Arrangement.spacedBy(8.dp, Alignment.Top)
        ) {
            item {
                if (uiState.activities.isEmpty()) {
                    NoRecordsRow()
                }
            }
            items(uiState.activities) { activity ->
                ActivityRow(
                    activity, navigateToActivityDetail, Modifier.animateItemPlacement(),
                )
            }
        }
    }
}

@Composable
fun FilterButton(
    filterType: FilterType,
    onClick: (FilterType) -> Unit,
    modifier: Modifier = Modifier
) {
    TextButton(modifier = modifier, onClick = {
        onClick(filterType)
    }) {
        Text(filterType.name)
    }
}

@Composable
fun NoRecordsRow(modifier: Modifier = Modifier) {
    Box(
        modifier = modifier.padding(10.dp).fillMaxWidth().border(
            width = 3.dp,
            color = MaterialTheme.colorScheme.secondary.copy(alpha = 0.5f),
            shape = RoundedCornerShape(25)
        ).clip(RoundedCornerShape(25))
            .background(color = MaterialTheme.colorScheme.secondaryContainer),
        contentAlignment = Alignment.Center
    ) {
        Text(
            modifier = Modifier.padding(vertical = 30.dp, horizontal = 10.dp),
            text = stringResource(Res.string.no_records),
            style = MaterialTheme.typography.headlineLarge,
            color = MaterialTheme.colorScheme.onSecondaryContainer,
            textAlign = TextAlign.Center
        )
    }
}

@Composable
fun ActivityRow(
    activity: SportActivity,
    navigateToActivityDetail: (SportActivity) -> Unit,
    modifier: Modifier = Modifier,
) {
    Column(
        modifier = modifier.fillMaxWidth().border(
            width = 2.dp,
            color = MaterialTheme.colorScheme.primary,
            shape = RoundedCornerShape(10.dp)
        ).clip(RoundedCornerShape(10.dp)).background(
            if (activity.isLocal) {
                MaterialTheme.colorScheme.primaryContainer
            } else MaterialTheme.colorScheme.tertiaryContainer
        ).clickable {
            navigateToActivityDetail(activity)
        }.padding(horizontal = 4.dp, vertical = 6.dp),
        verticalArrangement = Arrangement.spacedBy(2.dp, Alignment.Top),
    ) {
        Row(
            modifier = Modifier.fillMaxWidth().padding(horizontal = 8.dp),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Text(
                text = activity.name, color = MaterialTheme.colorScheme.onSurface,
            )
            val durationText = if (activity.durationInHours > 0) {
                stringResource(
                    Res.string.pretty_duration_long,
                    activity.durationInHours,
                    activity.durationInHoursAndMinutes ?: 0
                )
            } else stringResource(
                Res.string.pretty_duration_short, activity.duration ?: 0
            )
            Text(
                text = durationText, color = MaterialTheme.colorScheme.onSurface
            )
        }
        Row(
            modifier = Modifier.fillMaxWidth().padding(horizontal = 8.dp),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Row(
                horizontalArrangement = Arrangement.Start,
                verticalAlignment = Alignment.CenterVertically,
            ) {
                Text(
                    modifier = Modifier,
                    text = activity.place,
                    style = MaterialTheme.typography.bodyLarge,
                    color = MaterialTheme.colorScheme.onSurface
                )
            }
            RecordLocationChip(activity.isLocal)
        }
    }
}

@Composable
fun RecordLocationChip(isLocal: Boolean, modifier: Modifier = Modifier) {
    val text = if (isLocal) {
        stringResource(Res.string.local)
    } else stringResource(Res.string.remote)
    val icon = if (isLocal) {
        painterResource(Res.drawable.ic_local_24)
    } else painterResource(Res.drawable.ic_remote_24)
    SuggestionChip(modifier = modifier, enabled = false, onClick = {}, label = {
        Text(text)
    }, icon = {
        Icon(
            painter = icon, contentDescription = stringResource(Res.string.record_location)
        )
    })
}