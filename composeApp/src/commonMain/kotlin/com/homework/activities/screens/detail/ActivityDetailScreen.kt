package com.homework.activities.screens.detail

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowBack
import androidx.compose.material.icons.filled.Check
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.OutlinedTextFieldDefaults
import androidx.compose.material3.RadioButton
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusDirection
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.homework.activities.data.entities.SportActivity
import com.homework.activities.ui.navigation.Screen
import homeworkactivities.composeapp.generated.resources.Res
import homeworkactivities.composeapp.generated.resources.delete
import homeworkactivities.composeapp.generated.resources.duration
import homeworkactivities.composeapp.generated.resources.ic_local_24
import homeworkactivities.composeapp.generated.resources.ic_remote_24
import homeworkactivities.composeapp.generated.resources.local
import homeworkactivities.composeapp.generated.resources.minutes
import homeworkactivities.composeapp.generated.resources.navigate_back
import homeworkactivities.composeapp.generated.resources.no_value
import homeworkactivities.composeapp.generated.resources.place
import homeworkactivities.composeapp.generated.resources.record_location
import homeworkactivities.composeapp.generated.resources.remote
import homeworkactivities.composeapp.generated.resources.save
import homeworkactivities.composeapp.generated.resources.title
import kotlinx.coroutines.flow.collectLatest
import org.jetbrains.compose.resources.painterResource
import org.jetbrains.compose.resources.pluralStringResource
import org.jetbrains.compose.resources.stringResource

fun NavController.navigateToActivityDetail(sportActivity: SportActivity? = null) {
    val route = if (sportActivity == null) {
        "${Screen.ActivityDetail.route}/null/true"
    } else if (sportActivity.isLocal) {
        "${Screen.ActivityDetail.route}/${sportActivity.id}/${sportActivity.isLocal}"
    } else {
        "${Screen.ActivityDetail.route}/${sportActivity.remoteId}/${sportActivity.isLocal}"
    }
    navigate(route)
}

@Composable
fun ActivityDetailRoute(
    navigateBack: () -> Unit, viewModel: ActivityDetailViewModel
) {
    val uiState by viewModel.state.collectAsState()
    val errorState by viewModel.errorState.collectAsState()
    LaunchedEffect(Unit) {
        viewModel.jobDone.collectLatest { jobDone ->
            if (jobDone) {
                navigateBack()
            }
        }
    }

    ActivityDetailScreen(
        uiState = uiState,
        errorState = errorState,
        onTitleChanged = viewModel::onTitleChanged,
        onPlaceChanged = viewModel::onPlaceChanged,
        onDurationChanged = viewModel::onDurationChanged,
        onLocationChanged = viewModel::onLocationChanged,
        onDelete = viewModel::onDelete,
        onSave = viewModel::onSave,
        navigateBack = navigateBack
    )
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ActivityDetailScreen(
    uiState: ActivityDetailUiState,
    errorState: ErrorState,
    onTitleChanged: (String) -> Unit,
    onPlaceChanged: (String) -> Unit,
    onDurationChanged: (String) -> Unit,
    onLocationChanged: (Boolean) -> Unit,
    navigateBack: () -> Unit,
    onDelete: () -> Unit,
    onSave: () -> Unit
) {
    val title by remember {
        derivedStateOf {
            if (uiState.isEditMode) {
                "Úprava aktivity"
            } else "Nová aktivita"
        }
    }

    Column(modifier = Modifier.fillMaxSize()) {
        TopAppBar(title = {
            Text(
                text = title
            )
        }, navigationIcon = {
            IconButton(onClick = navigateBack) {
                Icon(
                    imageVector = Icons.AutoMirrored.Default.ArrowBack,
                    contentDescription = stringResource(Res.string.navigate_back),
                    tint = MaterialTheme.colorScheme.onPrimaryContainer
                )
            }
        }, actions = {
            if (uiState.sportActivity != null) {
                if (uiState.isEditMode) {
                    IconButton(onClick = onDelete) {
                        Icon(
                            imageVector = Icons.Default.Delete,
                            contentDescription = stringResource(Res.string.delete),
                            tint = MaterialTheme.colorScheme.onPrimaryContainer
                        )
                    }
                }
                IconButton(onClick = onSave) {
                    Icon(
                        imageVector = Icons.Default.Check,
                        contentDescription = stringResource(Res.string.save),
                        tint = MaterialTheme.colorScheme.onPrimaryContainer
                    )
                }
            }
        }, colors = TopAppBarDefaults.topAppBarColors(
            containerColor = MaterialTheme.colorScheme.primaryContainer,
            titleContentColor = MaterialTheme.colorScheme.primary,
        )
        )
        Column(
            modifier = Modifier.verticalScroll(rememberScrollState()).fillMaxSize().padding(10.dp)
        ) {
            if (uiState.isLoading) {
                LoadingIndicator()
            }
            ServerError(errorState.serverError)
            if (uiState.sportActivity != null) {
                SportActivityDetail(
                    sportActivity = uiState.sportActivity,
                    errorState = errorState,
                    isEditMode = uiState.isEditMode,
                    onTitleChanged = onTitleChanged,
                    onPlaceChanged = onPlaceChanged,
                    onDurationChanged = onDurationChanged,
                    onLocationChanged = onLocationChanged
                )
            }
        }
    }
}

@Composable
fun SportActivityDetail(
    sportActivity: SportActivity,
    errorState: ErrorState,
    isEditMode: Boolean,
    onTitleChanged: (String) -> Unit,
    onPlaceChanged: (String) -> Unit,
    onDurationChanged: (String) -> Unit,
    onLocationChanged: (Boolean) -> Unit,
) {
    val focusManager = LocalFocusManager.current
    OutlinedTextField(modifier = Modifier.fillMaxWidth(),
        value = sportActivity.name,
        onValueChange = onTitleChanged,
        label = {
            Text(text = stringResource(Res.string.title))
        },
        keyboardOptions = KeyboardOptions(
            capitalization = KeyboardCapitalization.Sentences,
            keyboardType = KeyboardType.Text,
            imeAction = ImeAction.Next
        ),
        keyboardActions = KeyboardActions(onNext = {
            focusManager.moveFocus(FocusDirection.Down)
        }),
        supportingText = {
            if (errorState.isTitleEmpty) {
                Text(
                    text = stringResource(Res.string.no_value)
                )
            }
        })
    OutlinedTextField(modifier = Modifier.fillMaxWidth(),
        value = sportActivity.place,
        onValueChange = onPlaceChanged,
        label = {
            Text(text = stringResource(Res.string.place))
        },
        keyboardOptions = KeyboardOptions(
            capitalization = KeyboardCapitalization.Sentences,
            keyboardType = KeyboardType.Text,
            imeAction = ImeAction.Next
        ),
        keyboardActions = KeyboardActions(onNext = {
            focusManager.moveFocus(FocusDirection.Down)
        }),
        supportingText = {
            if (errorState.isPlaceEmpty) {
                Text(
                    text = stringResource(Res.string.no_value)
                )
            }
        })
    OutlinedTextField(modifier = Modifier.fillMaxWidth(),
        value = sportActivity.duration?.toString() ?: "",
        onValueChange = {
            onDurationChanged(it)
        },
        label = {
            Text(text = stringResource(Res.string.duration))
        },
        suffix = {
            Text(text = pluralStringResource(Res.plurals.minutes, sportActivity.duration ?: 100))
        },
        keyboardOptions = KeyboardOptions(
            keyboardType = KeyboardType.Number, imeAction = ImeAction.Done
        ),
        supportingText = {
            if (errorState.isDurationInvalid) {
                Text(
                    text = stringResource(Res.string.no_value)
                )
            }
        })
    RecordLocationSelector(
        enabled = !isEditMode, //cant change location when in editing mode
        isLocal = sportActivity.isLocal, onLocationChanged = onLocationChanged
    )
}

@Composable
fun ServerError(serverError: String?) {
    AnimatedVisibility(
        visible = !serverError.isNullOrEmpty()
    ) {
        Row(
            modifier = Modifier.padding(4.dp)
                .background(color = MaterialTheme.colorScheme.errorContainer)
        ) {
            Text(
                text = serverError!!, color = MaterialTheme.colorScheme.onErrorContainer
            )
        }
    }
}

@Composable
fun RecordLocationSelector(
    enabled: Boolean, isLocal: Boolean, onLocationChanged: (Boolean) -> Unit
) {
    Column(
        modifier = Modifier.fillMaxWidth().padding(top = 4.dp).border(
            1.dp,
            color = Color.Gray,
            shape = OutlinedTextFieldDefaults.shape //match textfields border shape
        ).padding(horizontal = 5.dp, vertical = 8.dp)
    ) {
        Text(
            modifier = Modifier.fillMaxWidth(),
            text = stringResource(Res.string.record_location),
            color = MaterialTheme.colorScheme.onSecondaryContainer
        )
        RadioOptionRow(enabled = enabled,
            isLocal = isLocal,
            text = stringResource(Res.string.local),
            icon = painterResource(Res.drawable.ic_local_24),
            onLocationChanged = {
                onLocationChanged(true)
            })
        RadioOptionRow(enabled = enabled,
            isLocal = !isLocal,
            text = stringResource(Res.string.remote),
            icon = painterResource(Res.drawable.ic_remote_24),
            onLocationChanged = {
                onLocationChanged(false)
            })
    }
}

@Composable
fun RadioOptionRow(
    enabled: Boolean,
    isLocal: Boolean,
    text: String,
    icon: Painter,
    onLocationChanged: (Boolean) -> Unit
) {
    Row(
        modifier = Modifier.fillMaxWidth(),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.spacedBy(4.dp, Alignment.Start)
    ) {
        RadioButton(enabled = enabled, selected = isLocal, onClick = {
            onLocationChanged(true)
        })
        Icon(
            painter = icon, contentDescription = text
        )
        Text(
            text = text, color = MaterialTheme.colorScheme.onPrimaryContainer
        )
    }
}

@Composable
fun LoadingIndicator() {
    Box(
        modifier = Modifier.fillMaxSize().background(color = Color.Yellow),
        contentAlignment = Alignment.Center
    ) {
        Column {
            CircularProgressIndicator()
            Text(text = "loading")
        }
    }
}
