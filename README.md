## Úvod
Cílem zadání je vytvořit jednoduchou mobilní aplikaci, která bude zaznamenávat
sportovní výkony a ukládat je na backend (např. Firebase, vlastní řešení,...) a do
lokální databáze (např. Room, Realm, ...) dle volby uživatele.
Aplikace bude mít dvě obrazovky. První pro zadávání sportovních výkonů. Druhá
bude sloužit pro výpis již zadaných sportovních výkonů. Součástí úkolu je vymyslet
průchod aplikací, aby dával z uživatelského pohledu co největší smysl. Následně
vymyšlené navigační flow nejen implementovat, ale i vysvětlit, proč jsme zvolili
právě toto řešení. Aplikace musí správně fungovat jak v landscape, tak v portrait
módu. Snaž se použít jednotnou architekturu napříč celým projektem.
### Zadávání sportovního záznamu
Obrazovka pro zadávání sportovních výkonů bude implementovat následující
požadavky:
  - Zadání názvu sportovního výkonu.
  - Zadání místa konání sportovnímu výkonu.
  - Zadání délky trvání sportovního výkonu.
  - Výběr, zda položku uložit do lokální databáze nebo na backend.
  - Uložení položky sportovního výkonu do vybraného úložiště.
### Výpis již zadaných sportovních výkonů
Obrazovka pro výpis sportovních výkonů obsahuje:
  - Výpis vložených položek dle výběru (All | Local | Remote)
  - Barevně odlišené položky dle typu úložiště

Jakýkoliv další rozvoj aplikace, kterým bys chtěl ukázat své schopnosti, je vítán.


## Generated
This is a Kotlin Multiplatform project targeting Android, iOS.

* `/composeApp` is for code that will be shared across your Compose Multiplatform applications.
  It contains several subfolders:
  - `commonMain` is for code that’s common for all targets.
  - Other folders are for Kotlin code that will be compiled for only the platform indicated in the folder name.
    For example, if you want to use Apple’s CoreCrypto for the iOS part of your Kotlin app,
    `iosMain` would be the right folder for such calls.

* `/iosApp` contains iOS applications. Even if you’re sharing your UI with Compose Multiplatform,
  you need this entry point for your iOS app. This is also where you should add SwiftUI code for your project.


Learn more about [Kotlin Multiplatform](https://www.jetbrains.com/help/kotlin-multiplatform-dev/get-started.html)…
